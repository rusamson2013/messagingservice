# Messaging Service #

The Messaging Service is a RESTFUL Service that allows users to publish messages in an overloaded environment. it works by providing an endpoint where all incoming messages are stored in memory queue  before being consumed. it also allow to users to register , view their messages and do a like. 

![URLs Shortener API](images/main.png)


### Dependencies
- Java 8 plus 
  
### Tools Used 

- Spring Boot
	- For the main application  
- Maven 
	- as a build automation tool and packaging as a JAR/WAR
- ActiveMQ 
	- As an Inmemory Message Brocker
- H2 DB 
	- as an InMemory DB
- JPA-Hibernate 
	- for perisistance operations 
- Junit 5 + Mockito
	- as Testing Frameworks
- SonarLint & Eclipse
	- for Code Quality and Tests Coverage Stats
- Swagger
	- for RESTFul Endpoints rendering  *http://localhost:8080/swagger-ui.html*
- Spring Actuator
	- for Monitoring   *http://localhost:8080/actuator/index.html*


 
## HOW TO RUN ##


### How to run the APP Using the embedded tomcat as a microservice JAR ###

Below are steps on how to run the API 

- from the /app_war folder run  :   **java -jar messagingservice.war**
  
### How to run the APP from Tomcat as a WAR ###

Below are steps on how to run the API 

-	Find the **WAR** file under **/app_war** folder 
-	drag and drop the **WAR** file named **taskmanager.war** in a Tomcat **webapps** folder
-	If you tomcat runs on port 8080 , the navigate to the below URL : 
-	*http://localhost:8080/messagingservice/swagger-ui.html**   



## USABILITY OF THE SYSTEM ##

below is a short summary on the UI 

### SWAGGER REST interface ###

 Below are all accessible Endpoints of the application
 

 ![URLs Shortener API](images/pic1.png)
 
 ![URLs Shortener API](images/pic2.png)
 
 ![URLs Shortener API](images/pic3.png)
 
 ![URLs Shortener API](images/pic4.png)
 
 ![URLs Shortener API](images/pic5.png)
 
 ![URLs Shortener API](images/pic6.png)
 
![URLs Shortener API](images/pic7.png)
 
![URLs Shortener API](images/pic8.png)
 
![URLs Shortener API](images/pic9.png) 
 
![URLs Shortener API](images/pic10.png) 

![URLs Shortener API](images/pic11.png)

![URLs Shortener API](images/pic12.png)

![URLs Shortener API](images/pic12.png)


### Documentation
  - Java Documentation
    -  ![URLs Shortener API](images/javadoc.png)   
    
## CONFIGURATION ##


### ActiveMQ Inmemory configuration

In a case you want to stop using the Inmemory capability and use a defined port and server just dedicated for messaging, you can achieve it by changing the configuration The ActiveMQ settings 
The ActiveMQ settings are defined under  **application.properties** file located in the Resources folder  
  
### H2 Inmemory Database configuration

By Default ,I had to set the database to be in memory to avoid the write access issues that may rise. 
If there is a need of having it saved on the local machine , you can change the **application.properties** file located in the Resources folder as below 


## ADDITION INFO ##

the java doc can be found user the /doc folder 