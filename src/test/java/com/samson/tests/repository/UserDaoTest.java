package com.samson.tests.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test; 
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.samson.model.User;
import com.samson.repository.UserDao;

@RunWith(SpringRunner.class)
@SpringBootTest 
public class UserDaoTest {
 
	private  Logger logger = LoggerFactory.getLogger(UserDaoTest.class); 
	
	@Autowired(required=true)
	private UserDao userDao;   
	
	@Before
	public void setupMock() {
	    MockitoAnnotations.initMocks(this);   
	 }
	
	@Test
	public void testInsertOkay(){   
		User user =  new User();
		user.setUsername("username1");
		user.setPassword("password1");
		User savedUser = userDao.insert(user);  
		assertThat(user).isEqualTo(savedUser);	
	} 
	  
	@Test
	public void testFindOkay(){   
		User user =  new User();
		user.setUsername("username2");
		user.setPassword("password2");
	    userDao.insert(user);
		User foundUser =  userDao.findByUsername("username2");
		assertThat(user.getUsername()).isEqualTo(foundUser.getUsername()); 	
	} 	 
	
	@Test
	public void testUpdateOkay(){   
		User user = new User();
		user.setUsername("username3");
		user.setPassword("password3");
		User savedUder = userDao.insert(user);
		savedUder.setUsername("username33");
		userDao.update(savedUder);
		User foundUser =  userDao.find(savedUder.getId());
		assertThat(foundUser.getUsername()).isEqualTo("username33"); 
		assertThat(foundUser.getPassword()).isEqualTo("password3");  
	}
 
	@Test(expected = Exception.class)
	public void testDeleteOkay(){   
		User user = new User();
		user.setUsername("username4");
		user.setPassword("password4");
		User savedUser = userDao.insert(user);
		userDao.delete(savedUser);
		User deletedUser = userDao.find(savedUser.getId());
		assertThat(deletedUser).isNull(); 	
	}
	  
    //more tests are needed here but not required for this challenge 
}
