package com.samson.tests.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;


import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.samson.model.Message;
import com.samson.service.MessageService; 
import com.samson.tests.TestHelper; 


@RunWith(SpringRunner.class)
@SpringBootTest 
@ExtendWith(MockitoExtension.class)
public class MessageServiceTest {
 
	private  Logger logger = LoggerFactory.getLogger(MessageServiceTest.class);
	
	@Autowired(required=true)
	private MessageService messageService;  
	
	@Before
	public void setupMock() {
	    MockitoAnnotations.initMocks(this);	    
	 }
     
	@Test
	public void testMessagesGetCreateOkay(){  		
		Message message =  TestHelper.getMessage();
	    messageService.create(message);	
	    List<Message> savedMessages = messageService.findByUser("sender");
		assertThat(savedMessages.size()).isPositive();
	} 	
	
	@Test
	public void testGetAllMessagesOkayByRecipient(){  	
		List<Message> messages = TestHelper.getManyMessages();	
		for (Message message : messages) {
			messageService.create(message);
		}		
		List<Message> savedMessagesFroRecipientUser = messageService.findByUser("recipient");
		int numberOfTasks = savedMessagesFroRecipientUser.size();		
		assertThat(numberOfTasks).isEqualTo(3); 	
	} 	
	
	//more tests are needed here but not required for this challeng
}
