package com.samson.tests.controller;

import org.junit.Before;
import org.junit.Test; 
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.samson.model.MessageDto;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
 
@RunWith(SpringRunner.class)
@SpringBootTest
public class MesssageProducerTest {
	
	  private MockMvc mockMvc; 
	
	  @Autowired
	  private WebApplicationContext webApplicationContext;

		@Before
		public void setupMock() {
		    MockitoAnnotations.initMocks(this); 
		    this.mockMvc = webAppContextSetup(webApplicationContext).build();
		 } 
		
		@Test
		public void verifyIfMessagePublisherWorksOkay() throws Exception 
		{
			MessageDto message = new MessageDto();
			message.setSubject("The Subject");;
			message.setSender("mr_sender"); 
			message.setRecipient("ms_recipient");
			message.setContent("This is the message content");
			
			this.mockMvc.perform( MockMvcRequestBuilders
		      .post("/produce/message")
		      .content(asJsonString(message))
		      .contentType(MediaType.APPLICATION_JSON)
		      .accept(MediaType.APPLICATION_JSON));
			
			 //some assertions to to be implemented here
		}
		 
		public static String asJsonString(final Object obj) {
		    try {
		        return new ObjectMapper().writeValueAsString(obj);
		    } catch (Exception e) {
		        throw new RuntimeException(e);
		    }
		}
 
		//more tests are needed here but not required for this challenge  
}
