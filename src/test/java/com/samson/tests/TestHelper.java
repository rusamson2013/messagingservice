package com.samson.tests;

import java.util.ArrayList;
import java.util.List;

import com.samson.model.Message;
import com.samson.model.User; 

/**
* <h1>Tests Helper</h1>
* This class will be used to provide some functionalities to our tests
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/

public class TestHelper {
	//a static method to provide a user
	public static User getUser() {
		User user =  new User();
		user.setUsername("username");		
		user.setPassword("password");
		return user;
	}
	
	//a static method to provide a message
	public static Message getMessage() {
		Message message = new Message();
		message.setSubject("The Subject");
		message.setContent("The Content");
		message.setSender("sender");
	    message.setRecipient("uniquerecipient");
	    message.setLiked(false);
		return message;
	}
	
	//a static method to provide a list of messages

	public static List<Message> getManyMessages() {		
 
		Message message1 = new Message();
		message1.setSubject("The Subject");
		message1.setContent("The Content");
		message1.setSender("sender");
	    message1.setRecipient("recipient");
	    message1.setLiked(false);

		Message message2 = new Message();
		message2.setSubject("The Subject");
		message2.setContent("The Content");
		message2.setSender("sender");
	    message2.setRecipient("recipient");
	    message2.setLiked(false);
	    
		Message message3 = new Message();
		message3.setSubject("The Subject");
		message3.setContent("The Content");
		message3.setSender("sender");
	    message3.setRecipient("recipient");
	    message3.setLiked(false);
		
		List<Message> messages = new ArrayList<Message>();

		messages.add(message1);
		messages.add(message2);
		messages.add(message3);
		
		return messages;
	}
	 
}
