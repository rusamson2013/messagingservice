package com.samson.exception;

/**
* <h1>this is Just an example on Custom Exceptions handling NoMessageFoundException</h1> 
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/

public class NoMessageFoundException extends RuntimeException{
 
	private static final long serialVersionUID = 1L;
	
	private final String message;
	
	public NoMessageFoundException(String message) {
		super();
		this.message = message;
	}
	
	@Override
	public String getMessage() {
		return message;
	} 
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
