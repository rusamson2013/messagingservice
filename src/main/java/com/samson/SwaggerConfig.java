package com.samson;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;
import static com.google.common.base.Predicates.or;

/**
* <h1>Swagger CXonfiguration</h1>
*This is the Swagger configuration file to show all endpoints 
* @author  Samson Rukundo
* @version 1.0
* @since   2021-03-17
*/  

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
	public Docket postsApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("public-api")
				.apiInfo(apiInfo()).select().paths(postPaths()).build();
	}

	private Predicate<String> postPaths() {
		return or(regex("/message.*"), regex("/user.*"), regex("/produce.*"));
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Messaging Service")
				.description("Messaging Service built using Spring Boot ,InMemory AciveMQ, InMemory H2DB, JPA-Hibernate, Junit 5 and Mockito ") 
				.contact("rusamson@hotmail.com").license("Samson Rukundo")
				.licenseUrl("rusamson@hotmail.com").version("1.0").build();
	}
	 
}
