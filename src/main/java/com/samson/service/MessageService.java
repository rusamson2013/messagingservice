package com.samson.service;

import java.util.List;

import com.samson.model.Message;

/**
* <h1>User Service Interface</h1>
* This gives a list of methods that will need to be implemented
* 
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/

public interface MessageService {

	/**
	* This method is used to create a Message.  
	* @param message, The message to save to the DB 
	*/
	public void create(Message message);
	
	/**
	* This method is used to return a Message based on the id.  
	* @param int, This is the id of the message
	* @return Message, This is the freshly updated Message.
	*/
	public Message update(Message message);
	
	/**
	* This method is used to return a Message based on the id.  
	* @param int, This is the id of the message
	* @return Message, This is the Message.
	*/
	public Message get(int id);

	/**
	* This method is used to delete a Message.  
	* @param id, This is the id of the message to delete
	* @return void, nothing will be returned.
	*/	
	public void delete(int id);

	/**
	* This method is used to return all Messages.  
	* @return List, This is the list of all messages.
	*/
	public List<Message> getAllMessages();
 
	 /**
	 * This method is used to return all Messages of one user by the username.  
	 * @param username, This is the username of the User  
	 * @return List, This is the found Messages.
	 */
	public List<Message> findByUser(String username);

	 /**
	 * This method is used to return all Messages between 2 users by their usernames.  
	 * @param username1, This is the username of User 1
	 * @param username2, This is the username of User 2 
	 * @return List, This is the found Messages  between 2 users.
	 */
	public List<Message> findByUsers(String username1, String username2);

}
