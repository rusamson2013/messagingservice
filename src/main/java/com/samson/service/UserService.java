package com.samson.service;

import java.util.List;

import com.samson.model.User;

/**
* <h1>User User Interface</h1>
* This gives a list of methods that will need to be implemented
* 
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/

public interface UserService {

	/**
	* This method is used to create a User.  
	* @param user, The user to save to the DB
	* @return User, This is the freshly saved User.
	*/
	public User create(User user);
	
	/**
	* This method is used to return a User based on the id.  
	* @param int, This is the id of the user
	* @return User, This is the freshly updated User.
	*/
	public User update(User user);
	
	/**
	* This method is used to return a User based on the id.  
	* @param int, This is the id of the user
	* @return User, This is the User.
	*/
	public User get(int id);

	/**
	* This method is used to delete a User.  
	* @param id, This is the id of the user to delete
	* @return void, nothing will be returned .
	*/	
	public void delete(int id);

	/**
	* This method is used to return all Users.  
	* @return List, This is the list of all users.
	*/
	public List<User> getAllUsers();

}
