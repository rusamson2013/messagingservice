package com.samson.service;
 
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.samson.model.User;
import com.samson.repository.UserDao; 

/**
* <h1>User Service Class</h1>
* This class is for the servicing capabilities 
* It takes  care of all the Business Logic
* 
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/

@Service 
@Transactional
public class UserServiceImpl implements UserService {

	private  Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserDao userDao;	 
    
	/**
	 * This method is used to save a new User.  
	 * @param user, This is the only parameter to be used
	 * @return User, This is the freshly created User.
	 */
	@Override
	public User create(User user){ 
		String username = user.getUsername().trim().toLowerCase(); 
		user.setUsername(username); 
		user.setId(0);
		if(userDao.findByUsername(user.getUsername()) == null) {
			return userDao.insert(user);
		}else {
			//handle by Exception or other logic
			logger.error(" couldn't create the user because the user already exist");
			return user;
		} 
	} 

	/**
	 * This method is used to update a given User.  
	 * @param user, This is the only parameter to be used
	 * @return User, This is the freshly updated User.
	 */
	@Override
	public User update(User user) { 
		
		String username = user.getUsername().trim().toLowerCase(); 
		user.setUsername(username); 
		if(userDao.findByUsername(user.getUsername()) != null) {
			return userDao.update(user);
		}else {
			//handle by Exception or other logic
			logger.error(" couldn't update the user because the user doesn't exist");
			return user;
		} 
		
	}

	/**
	 * This method is used to return a User based on the ID.  
	 * @param id, This is the only parameter to be used
	 * @return User, This is the User returned.
	 */
	@Override
	public User get(int id) { 
		return userDao.find(id);
	}

	/**
	 * This method is used to delete a User.  
	 * @param id, This is the id of the user to be deleted
	 */
	@Override
	public void delete(int id) {   
		User user = userDao.find(id);
		if(user != null) {
			 userDao.delete(user);
		}else {
			//handle by Exception or other logic
			logger.error(" couldn't delete the user because the user doesn't exist"); 
		} 
	}

	/**
	 * This method is used to get All users from the DB.  
	 * @return List, the list of all users.
	 */
	@Override
	public List<User> getAllUsers() {
		return userDao.findAll();
	}  
 
}
