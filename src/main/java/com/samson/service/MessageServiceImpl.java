package com.samson.service;
 
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.samson.exception.NoMessageFoundException;
import com.samson.model.Message;
import com.samson.repository.MessageDao;
import com.samson.repository.UserDao; 
/**
* <h1>Message Service Class</h1>
* This class is for the servicing capabilities 
* It takes  care of all the Business Logic
* 
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/

@Service 
@Transactional
public class MessageServiceImpl implements MessageService {
    
	private  Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);
	
	@Autowired
	private MessageDao messageDao;

	@Autowired
	private UserDao userDao;
    
	/**
	 * This method is used to save a new Message.  
	 * @param message, This is the only parameter to be used
	 */
	@Override
	public void create(Message message) {      
	     messageDao.insert(message); 
	} 

	/**
	 * This method is used to update a given Message.  
	 * @param message, This is the only parameter to be used
	 * @return Message, This is the freshly updated Message.
	 */
	@Override
	public Message update(Message message) { 
		 
		if(messageDao.find(message.getId()) != null) {
			messageDao.update(message);
		}else { 
			logger.error("message to update not found not found");
			throw new NoMessageFoundException(" no message to update found ");
		}		
		return this.get(message.getId());
		 
	}

	/**
	 * This method is used to return a Message based on the ID.  
	 * @param id, This is the only parameter to be used
	 * @return Message, This is the Message returned.
	 */
	@Override
	public Message get(int id) { 	
		
		Message message = messageDao.find(id);		
		if(message != null) {			
			return message; 
		}
		else {
			logger.error(" no message found  ");
			throw new NoMessageFoundException(" no message found  ");
		} 
	}

	/**
	 * This method is used to delete a Message.  
	 * @param id, This is the id of the message to be deleted
	 */
	@Override
	public void delete(int id) { 
	 	if(messageDao.find(id) != null) {
			messageDao.delete(id);
		}else { 
			logger.error("message to delete not found");
			throw new NoMessageFoundException(" no message found to delete ");
		}  
	}

	/**
	 * This method is used to get All messages from the DB.  
	 * @return List, the list of all messages.
	 */
	@Override
	public List<Message> getAllMessages() {
		return messageDao.findAll();
	}  
 
	/**
	 *This method is used to return all Messages of one user by the username.  
	 * @param username, This is the username of the User  
	 * @return List, This is the found Messages.
	 */
	@Override
	public List<Message> findByUser(String username) {
		return messageDao.findByUser(username.trim().toLowerCase());
	}  
	
	/**
	 * This method is used to get All messages from the DB between Users.  
	 * @return List, the list of all messages between Users.
	 */
	@Override
	public List<Message> findByUsers(String username1, String username2) {
		return messageDao.findByUsers(username1, username2);
	}  
	
}
