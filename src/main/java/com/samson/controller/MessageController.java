package com.samson.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.samson.model.Message;
import com.samson.model.MessageDto;
import com.samson.service.MessageService; 

/**
* <h1>Messages Controller</h1>
*This class deals with all messages related CRUDs functionalities.  
* @author  Samson Rukundo
* @version 1.0
* @since   2021-03-17
*/  

@RestController
@RequestMapping("/message")
public class MessageController {

	
	@Autowired
	private MessageService messageService;   

    @Autowired
    private ModelMapper modelMapper;
    
    @Autowired 	 
    private Message message;
    
	  /**
	  * This method is used to create a message and return it. .  
	  * @param message, This the Message object 
	  */

	@PostMapping(value = "/add", produces = "application/json")
	public void createMessage(@Valid @ModelAttribute("MessageDto") MessageDto messageDto) {
	  //making a copy from the DTO object and initializing some values before sending the message object to the service layer
	  //new messages to persist should have an id set to 0 as default value
	  modelMapper.map(messageDto, message);
	  message.setId(0);
	  message.setLiked(false);
	  messageService.create(message);
	}
	
	  /**
	  * This method returns all Messages from the DB.  
	  * @return List, Return the List of all Messages .
	  */  
	@GetMapping(value = "/all", produces = "application/json")
	public ResponseEntity<List<Message>> getAllMessages() { 
	return new ResponseEntity<>(messageService.getAllMessages(), HttpStatus.OK);
	} 
	
	  /**
	  * This method is used to get a Message by id.  
	  * @param id, This is the id of the message  
	  * @return Message, Returns the requested Message.
	  */
	@GetMapping("/{id}")
	public ResponseEntity<Message> getMessageById(@PathVariable("id") int id) {  
		 return new ResponseEntity<>( messageService.get(id), HttpStatus.OK);
	}   

	  /**
	  * This method is used to get a Message by id.  
	  * @param id, This is the id of the message  
	  * @return Message, Returns the requested Message.
	  */
	@GetMapping("user/{username}")  
	public ResponseEntity<List<Message>> getMessagesByUser(@PathVariable("username") String username) {  
		 return new ResponseEntity<>( messageService.findByUser(username), HttpStatus.OK);
	}   

	  /**
	  * This method is used to get a Message by id.  
	  * @param id, This is the id of the message  
	  * @return Message, Returns the requested Message.
	  */
	@GetMapping("users/{firstusername}/{secondusername}")  
	public ResponseEntity<List<Message>> getMessagesBetweenUsers(@PathVariable("firstusername") String firstusername, @PathVariable("secondusername") String secondusername) {  
		 return new ResponseEntity<>( messageService.findByUsers(firstusername, secondusername), HttpStatus.OK);
	} 
	
	  /**
	  * This method is used to update a message and return it. .  
	  * @param message, This the Message object
	  * @return Message, Return the freshly created message .
	  */
	
	@PutMapping(value = "/update", produces = "application/json")
	public ResponseEntity<Message>  updateMessage(@Valid @ModelAttribute("Message") Message message) {  
		return new ResponseEntity<>( messageService.update(message), HttpStatus.OK);
	}
	
	 /**
	 * This method is used to update a message and return it. .  
	 * @param message, This the Message object
	 * @return Message, Return the freshly created message .
	 */
	
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")
	public  void deleteMessage(@PathVariable("id") int id) {  
		  messageService.delete(id);
	}
}