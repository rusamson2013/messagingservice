package com.samson.controller;
 
import java.util.List; 
 
import javax.validation.Valid;
  
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping; 
  
import com.samson.model.User;
import com.samson.service.UserService; 

/**
* <h1>User Controller</h1>
* This is the user controller
* it gives an entry point to all internal users operations 
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/

@Controller
@RequestMapping("user")
public class UserController { 
	
	@Autowired
	private UserService userService;    

	   /**
	   * This method is used to create a user and return that user. .  
	   * @param user, This the User object 
	   * @return Task, Return the freshly created user .
	   */
	
    @PostMapping(value = "/add", produces = "application/json")
	public ResponseEntity<User>  createUser(@Valid @ModelAttribute("User") User user) {  
		return new ResponseEntity<>( userService.create(user), HttpStatus.OK);
 	}
    
	   /**
	   * This method returns all Users from the DB.  
	   * @return List, Return the List of all Users .
	   */  
    @GetMapping(value = "/all", produces = "application/json")
	public ResponseEntity<List<User>> getAllUsers() { 
	return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
	} 
 
	   /**
	   * This method is used to get a User by id.  
	   * @param id, This is the id of the user  
	   * @return User, Returns the requested User.
	   */
     @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") int id) {  
    	 return new ResponseEntity<>( userService.get(id), HttpStatus.OK);
    }   
 
	   /**
	   * This method is used to update a user and return it. .  
	   * @param user, This the User object 
	   * @return User, Return the freshly created user .
	   */
	
     @PutMapping(value = "/update", produces = "application/json")
	public ResponseEntity<User>  updateUser(@Valid @ModelAttribute("User") User user) {  
		return new ResponseEntity<>( userService.update(user), HttpStatus.OK);
	}
  
	  /**
	  * This method is used to update a user and return the user. .  
	  * @param user, This the User object 
	  * @return User, Return the freshly created user .
	  */
	
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")
	public  void deleteUser(@PathVariable("id") int id) {  
		  userService.delete(id);
	}

}