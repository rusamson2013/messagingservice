package com.samson.messaging;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import javax.jms.Queue;

/**
* <h1>In Memory Active MQ Configuration</h1>
*This is the main place for the ActiveMQ configuration
* @author  Samson Rukundo
* @version 1.0
* @since   2021-03-17
*/

@EnableJms
@Configuration
public class ActiveMQConfiguration {

	  /**
	  * the in memory ActiveMQQueue 
	  * 
	  */ 
    @Bean   
    public Queue createQueue(){
        return new ActiveMQQueue("local.inmemory.queue");
    }
     
	  /**
	  * Serialize message content to JSON using TextMessage  
	  * 
	  */  	    
	@Bean
	public MessageConverter jacksonJmsMessageConverter() {
	    MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
	    converter.setTargetType(MessageType.TEXT);
	    converter.setTypeIdPropertyName("_type");
	    return converter;
	}
}