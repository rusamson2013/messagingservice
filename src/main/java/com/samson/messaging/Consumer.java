package com.samson.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.samson.model.Message; 
import com.samson.service.MessageService; 

/**
* <h1>Main Consumer Class</h1>
*This is the main place for consuming messages from the queue
* @author  Samson Rukundo
* @version 1.0
* @since   2021-03-17
*/

@Component
public class Consumer{

	Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MessageService messageService;

	/**
	* This method is used to consume  published messages from the Message queue. 
	* @param message, This the Message object  
	*/
	@JmsListener(destination = "local.inmemory.queue")
	public void onMessage(Message message) {
	  messageService.create(message);
	  logger.info("the massage has been consumed !");
	}

}