package com.samson.messaging;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.jms.core.JmsTemplate; 
import org.springframework.web.bind.annotation.ModelAttribute; 
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.samson.model.Message;
import com.samson.model.MessageDto;

import javax.jms.Queue;
import javax.validation.Valid;

/**
* <h1>Main Publisher Class</h1>
*This is the main place for clients to produce messages, those messages will be added to the queue
* @author  Samson Rukundo
* @version 1.0
* @since   2021-03-17
*/

@RequestMapping("/produce")
@RestController
public class Producer {

	@Autowired    
	private JmsTemplate jmsTemplate; 
	
	@Autowired    
	private Queue queue;

    @Autowired
    private ModelMapper modelMapper;
    
    @Autowired 	 
    private Message message;

	/**
	* This method is used by a client to publish messages to the Message queue. 
	* @param message, This the Message object  
	*/
	@PostMapping("/message")
	public String sendMessage(@Valid @ModelAttribute("MessageDto")  MessageDto messageDto) { 
	//in addition to HTTP Status Code , the message sender will know if it was a successful or a failed one
	 try {
		modelMapper.map(messageDto, message); 
	    jmsTemplate.convertAndSend(queue, message);
	    return "message sent successfuly !";
	 }catch(Exception e){
		e.printStackTrace();
		return "Error sending message !";
	 }
	}
 
}