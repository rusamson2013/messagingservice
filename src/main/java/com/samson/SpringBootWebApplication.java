package com.samson; 
 
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication; 
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

/**
* <h1>MessagingService</h1>
*This it is a messaging system that exposes an endpoint where clients can publish messages
*It provides the business logic between users and the messages received
*It is a Spring Boot service that uses In Memory messages broker (ActiveMQ) and InMemory DB (H2),
* It also uses Hibernate for persistence tasks and JUnit 5 + Mockito
* @author  Samson Rukundo
* @version 1.0
* @since   2021-03-17
*/  

@SpringBootApplication
public class SpringBootWebApplication extends SpringBootServletInitializer {
	  
    private static Logger loggerInfo = LoggerFactory.getLogger(SpringBootWebApplication.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SpringBootWebApplication.class);
	}

	public static void main(String[] args){
		SpringApplication.run(SpringBootWebApplication.class, args);
		loggerInfo.info("Application started");
	}
	
	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}

}