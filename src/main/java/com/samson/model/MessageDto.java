package com.samson.model;
 
import javax.validation.constraints.NotNull;
 
/**
* <h1>MessageDto POJO</h1>
* This is will be used to hold the message from the publisher and validations
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/

public class MessageDto { 

	private int id; 
	@NotNull(message="subject should not be null")
	private String subject;
	@NotNull(message="content should not be null")
	private String content;  
	@NotNull(message="sender should not be null")
	private String sender;  
	@NotNull(message="recipient should not be null")
	private String recipient;
	
	public MessageDto(){
		this.id = 0;
	}
	
	public int getId() {
		return id;
	}
	 
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	
	@Override
	public String toString() {
		return "MessageDto [subject=" + subject + ", content=" + content + ", sender=" + sender + ", recipient=" + recipient
				+ "]";
	} 
}
