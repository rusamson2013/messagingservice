package com.samson.model;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
 
import org.springframework.stereotype.Component; 

/**
* <h1>Message Entity</h1>
* This is will be used for persistence by the JPA-Hibernate , it holds as well some NamedQuery
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/ 

@Component
@Entity 
@NamedQueries({
    @NamedQuery(name="findAllMessages",
                query="SELECT m FROM Message m"), 
    @NamedQuery(name="findByUser",
    			query="SELECT m FROM Message m WHERE m.sender = :username OR m.recipient = :username"),
    @NamedQuery(name="findByUsers",
    			query="SELECT m FROM Message m WHERE ( m.sender = :username1 OR m.sender = :username2 ) AND ( m.recipient = :username1 OR m.recipient = :username2 )"),   
}) 


@Table(name = "Message")
public class Message { 
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id; 
	private String subject;
	private String content;  
	private String sender;  
	private String recipient;  
	private boolean liked;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	 
	public boolean isLiked() {
		return liked;
	}
	public void setLiked(boolean liked) {
		this.liked = liked;
	}
	@Override
	public String toString() {
		return "Message [id=" + id + ", subject=" + subject + ", content=" + content + ", sender=" + sender + ", recipient="
				+ recipient + ", liked=" + liked + "]";
	}

}
