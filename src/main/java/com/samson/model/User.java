package com.samson.model;
 
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
 
/**
* <h1>User Entity</h1>
* This is will be used for persistence and has also some namedQuery 
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/

@Entity 
@NamedQueries({
    @NamedQuery(name="findAllUsers",
                query="SELECT u FROM User u"),
    @NamedQuery(name="findUserByUsername",
                query="SELECT u FROM User u WHERE u.username = :username"),
})

@Table(name = "User")
public class User { 
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id; 
	private String username;
	private String password;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + "]";
	}   

}
