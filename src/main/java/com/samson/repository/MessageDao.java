package com.samson.repository;

import java.util.List;

import com.samson.model.Message; 

/**
* <h1>Message Data Access Interface</h1>
* This gives a list of methods that will need to be implemented
* 
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/

public interface MessageDao {
	
	   /**
	   * This method is used insert a Message to the DB.  
	   * @param message, This is the only parameter to be used 
	 * @return 
	   */
	public void insert(Message message);

	   /**
	   * This method is used to return a Message form the DB.  
	   * @param id, This is the ID of the Message that needs to be found
	   * @return Message, This is the found Message.
	   */
	public Message find(int id);

	   /**
	   * This method that update the Message .  
	   * @param Message, a message should be passed as a parameter 
	   */
	public void update(Message message);

	   /**
	   * This method is used to delete a Message from the DB.  
	   * @param message, you pass in the task that needs to be deleted 
	   */
	public void delete(int id); 
	
	   /**
	   * This method is used to return all Messages existing in the DB.   
	   * @return List, This is the List of all Messages from the DB.
	   */
	public List<Message> findAll();

	   /**
	   * This method is used to return all Messages by a given username.   
	   * @return List, This is the List of all Messages from the DB for the given username.
	   */
	public List<Message> findByUser(String username);

	   /**
	   * This method is used to return all Messages between 2 users.   
	   * @return List, This is the List of all Messages from the DB between 2 users.
	   */
	List<Message> findByUsers(String username1, String username2);

 
 
}
