package com.samson.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.samson.model.Message; 

/**
* <h1>Message Data Access Class</h1>
* This deals with the persistence 
* It takes  care of all the DB CRUIDs on the Message table
* 
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/

@Repository
@Transactional
public class MessageDaoImpl  implements MessageDao {
	
	private  Logger logger = LoggerFactory.getLogger(MessageDaoImpl.class);
	
	@PersistenceContext
	private EntityManager entityManager;

	   /**
	   * This method is used insert a Message to the DB.  
	   * @param message, This is the only parameter to be used
	   * @return int, This is the ID of the inserted Message.
	   */
	@Override	
	public void insert(Message message) {
	    logger.info("persisting a Message to the DB");
		 entityManager.persist(message); 
	}

	   /**
	   * This method is used to return a Message form the DB.  
	   * @param id, This is the ID of the Message that needs to be found
	   * @return Message, This is the found Message.
	   */
	@Override
	public Message find(int id) {
		logger.info("finding a Message by id");
		return entityManager.find(Message.class, id);
	}

	   /**
	   * This method update a Message .  
	   * @param message, The message should be passed as the parameter 
	   */
	@Override
	public void update(Message message) {
		logger.info("Updating the Message");
		entityManager.merge(message);  
 	}

	   /**
	   * This method is used to delete a Message from the DB.  
	   * @param message, you pass in the message that needs to be deleted 
	   */
	@Override
	public void delete(int id) {
		logger.info("deleting a Message");
		 entityManager.remove(this.find(id)); 			 
	} 
	
	   /**
	   * This method is used to return all Messages existing in the DB.   
	   * @return List, This is the List of all Messages from the DB.
	   */
	@Override	
	public List<Message> findAll() { 
		    logger.info("finding all Messages from the DB");
			Query query = entityManager.createNamedQuery("findAllMessages", Message.class);
		return query.getResultList();
	}

	   /**
	   * This method is used to return all Messages of one user by the username.  
	   * @param username, This is the username of the User  
	   * @return List, This is the found Messages.
	   */
	@Override
	public List<Message>  findByUser(String username) { 
		logger.info("finding all Messages by username");
	    Query query = entityManager.createNamedQuery("findByUser", Message.class);
			  query.setParameter("username", username); 
			  return query.getResultList(); 
	}
		
	   /**
	   * This method is used to return all Messages between 2 users by their usernames.  
	   * @param username1, This is the username of User 1
	   * @param username2, This is the username of User 2 
	   * @return List, This is the found Messages  between 2 users.
	   */
	@Override
	public List<Message>  findByUsers(String username1, String username2) { 
		logger.info("finding all Messages between 2  users");
	    Query query = entityManager.createNamedQuery("findByUsers", Message.class);
			  query.setParameter("username1", username1);
			  query.setParameter("username2", username2); 
			  return query.getResultList(); 
	}
}