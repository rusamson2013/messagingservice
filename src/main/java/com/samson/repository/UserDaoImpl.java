package com.samson.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.samson.model.User; 

/**
* <h1>User Data Access Class</h1>
* This deals with the persistence 
* It takes  care of all the DB CRUIDs 
* 
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/

@Repository
@Transactional
public class UserDaoImpl  implements UserDao {
	
	private  Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);
	
	@PersistenceContext
	private EntityManager entityManager;

	   /**
	   * This method is used insert a User to the DB.  
	   * @param user, This is the only parameter to be used
	   * @return User, This is the ID of the inserted User.
	   */
	@Override	
	public User insert(User user) {
	    logger.info("persisting a User to the DB");
		entityManager.persist(user);
		return this.findByUsername(user.getUsername());
	}

	   /**
	   * This method is used to return a User from the DB.  
	   * @param id, This is the ID of the User that needs to be found
	   * @return User, This is the found User.
	   */
	@Override
	public User find(int id) {
		logger.info("finding a User by id");
		return entityManager.find(User.class, id);
	}

	   /**
	   * This method is used to return a User from the DB by username.  
	   * @param username, This is the username of the User that needs to be found
	   * @return User, This is the found User.
	   */
	@Override
	public User findByUsername(String username) { 
		logger.info("finding a User by username");
	    Query query = entityManager.createNamedQuery("findUserByUsername", User.class);
			  query.setParameter("username", username); 
			  if(query.getResultList().isEmpty() ) {
				  return null;
			  }else {
				  return (User)query.getResultList().get(0);
			 }	
	}
 
	   /**
	   * This method update a User .  
	   * @param user, The user should be passed as the parameter 
	   */
	@Override
	public User update(User user) {
		logger.info("Updating the User");
		return entityManager.merge(user);  
 	}

	   /**
	   * This method is used to delete a User from the DB.  
	   * @param user, you pass in the user that needs to be deleted 
	   */
	@Override
	public void delete(User user) {
		logger.info("deleting a User");
		 entityManager.remove(user); 			 
	} 
	
	   /**
	   * This method is used to return all Messages existing in the DB.   
	   * @return List, This is the List of all Messages from the DB.
	   */
	@Override	
	public List<User> findAll() { 
		    logger.info("finding all Users from the DB");
			Query query = entityManager.createNamedQuery("findAllUsers", User.class);
		return query.getResultList();
	}

}