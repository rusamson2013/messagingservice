package com.samson.repository;

import java.util.List;

import com.samson.model.Message;
import com.samson.model.User;

/**
* <h1>User Data Access Interface</h1>
* This gives a list of methods that will need to be implemented
* 
* @author  Samson Rukundo
* @version 1.0
* @since   17-03-2021
*/

public interface UserDao {
	
	   /**
	   * This method is used insert a User to the DB.  
	   * @param user, This is the only parameter to be used
	   * @return int, This is the ID of the inserted User.
	   */
	public User insert(User user);

	   /**
	   * This method is used to return a User form the DB.  
	   * @param id, This is the ID of the User that needs to be found
	   * @return User, This is the found User.
	   */
	public User find(int id);

	   /**
	   * This method is used to return a User form the DB by username.  
	   * @param username, This is the Username of the User that needs to be found
	   * @return User, This is the found User.       
       */
	public User findByUsername(String username);
	
	   /**
	   * This method that update the User .  
	   * @param User, a user should be passed as a parameter 
	   */
	public User update(User user);

	   /**
	   * This method is used to delete a User from the DB.  
	   * @param user, you pass in the user that needs to be deleted 
	   */
	public void delete(User user); 
	
	   /**
	   * This method is used to return all Users existing in the DB.   
	   * @return List, This is the List of all Users from the DB.
	   */
	public List<User> findAll();
}
